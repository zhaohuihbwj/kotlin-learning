package pers.zh.kotlin.service;

import com.github.pagehelper.PageHelper;
import pers.zh.kotlin.mapper.JavaDemoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class JavaDemoService {
    @Autowired
    private JavaDemoMapper javaDemoMapper;

    public List<Map<String,Object>> queryAll(){
        //分页
        PageHelper.startPage(1,2);
        return javaDemoMapper.queryAll();
    }
}