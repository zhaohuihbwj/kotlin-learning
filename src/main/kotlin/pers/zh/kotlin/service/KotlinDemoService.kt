package pers.zh.kotlin.service

import com.github.pagehelper.PageHelper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import pers.zh.kotlin.mapper.KotlinDemoMapper

@Service
class KotlinDemoService {
    @Autowired
    val kotlinDemoMapper: KotlinDemoMapper? = null

    fun queryAll(): List<Map<String, Any>> {
        //分页
        PageHelper.startPage<Any>(1, 2)
        return kotlinDemoMapper!!.queryAll()
    }
}



