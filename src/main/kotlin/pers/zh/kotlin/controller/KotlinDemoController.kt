package pers.zh.kotlin.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pers.zh.kotlin.service.KotlinDemoService

@RestController
@RequestMapping("/kotlin")
class KotlinDemoController{
    @Autowired
    val kotlinDemoService: KotlinDemoService? = null

    @RequestMapping("/queryAll")
    fun queryAll(): List<Map<String, Any>> {
        return kotlinDemoService!!.queryAll()
    }
}




