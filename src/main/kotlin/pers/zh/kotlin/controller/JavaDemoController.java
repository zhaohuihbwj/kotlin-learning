package pers.zh.kotlin.controller;

import pers.zh.kotlin.service.JavaDemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/java")
public class JavaDemoController {
    @Autowired
    private JavaDemoService javaDemoService;

    @RequestMapping("/queryAll")
    public List<Map<String,Object>> queryAll() {
        return javaDemoService.queryAll();
    }

}
