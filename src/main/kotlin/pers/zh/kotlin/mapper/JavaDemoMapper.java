package pers.zh.kotlin.mapper;

import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface JavaDemoMapper {

    @Select("select * from Demo")
    List<Map<String,Object>> queryAll();

}
