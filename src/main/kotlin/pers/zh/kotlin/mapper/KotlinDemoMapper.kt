package pers.zh.kotlin.mapper

import org.apache.ibatis.annotations.Select

interface KotlinDemoMapper {
    @Select("select * from Demo")
    fun queryAll(): List<Map<String, Any>>
}
